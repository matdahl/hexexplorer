/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * HexExplorer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import QtQuick.Controls 2.2 as QC
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3 as LTP //for Dialogs

Page {
    id: settingsPage
    anchors.fill: parent

    /* defining main slider values as properties */

    // Background Values
    property int backgroundRedValue: backgroundRedSlider.value
    property int backgroundGreenValue: backgroundGreenSlider.value
    property int backgroundBlueValue: backgroundBlueSlider.value

    property string backgroundRedString: (backgroundRedValue < 16)  ? "0" + backgroundRedValue.toString(16) : backgroundRedValue.toString(16)
    property string backgroundGreenString: (backgroundGreenValue < 16)  ? "0" + backgroundGreenValue.toString(16) : backgroundGreenValue.toString(16)
    property string backgroundBlueString: (backgroundBlueValue < 16)  ? "0" + backgroundBlueValue.toString(16) : backgroundBlueValue.toString(16)

    property string newBackgroundColour: "#" + backgroundRedString + backgroundGreenString + backgroundBlueString

    // Ink Values
    property int inkRedValue: inkRedSlider.value
    property int inkGreenValue: inkGreenSlider.value
    property int inkBlueValue: inkBlueSlider.value

    property string inkRedString: (inkRedValue < 16) ? "0" + inkRedValue.toString(16): inkRedValue.toString(16)
    property string inkGreenString: (inkGreenValue < 16) ? "0" + inkGreenValue.toString(16): inkGreenValue.toString(16)
    property string inkBlueString: (inkBlueValue < 16) ? "0" + inkBlueValue.toString(16): inkBlueValue.toString(16)

    property string newInkColour: "#" + inkRedString + inkGreenString + inkBlueString

    // Default Background and Ink Values
    property int defaultBackgroundRed: 0x2c
    property int defaultBackgroundGreen: 0x00
    property int defaultBackgroundBlue: 0x1e

    property int defaultInkRed: 0xe9
    property int defaultInkGreen: 0x54
    property int defaultInkBlue: 0x20

    /* end of defining main slider values as properties */

    header: PageHeader {
        id: header
        title: i18n.tr('Settings')

        StyleHints {
            foregroundColor: appSettings.persistentInkColor
            backgroundColor: appSettings.persistentBackgroundColor
            dividerColor: Qt.darker(backgroundColour)
        }
    }

    Component.onCompleted: {
        backgroundRedSlider.value = appSettings.persistentBackgroundRedValue
        backgroundGreenSlider.value = appSettings.persistentBackgroundGreenValue
        backgroundBlueSlider.value = appSettings.persistentBackgroundBlueValue

        inkRedSlider.value = appSettings.persistentInkRedValue
        inkGreenSlider.value = appSettings.persistentInkGreenValue
        inkBlueSlider.value = appSettings.persistentInkBlueValue
    }

    Rectangle {
        id: quickChangeSection
        width: parent.width
        height: parent.height/9
        color: appSettings.persistentBackgroundColor
        anchors.top: header.bottom

        Rectangle {
            id: hexRectangle
            width: parent.width - units.gu(2)
            height: parent.height - units.gu(2)
            anchors.centerIn: parent
            radius: 8
            color: newBackgroundColour
            border.color: newBackgroundColour == backgroundColor ? inkColour : "transparent"
            border.width: 3

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if (calcHexDifference() >= 1.5) {
                        setNewColors()
                    } else {
                        PopupUtils.open(contrastErrorDialog)
                    }
                }

        } //MouseArea

        } //hexRectangle

        Label {
            id: hexDiffLabel
            text: " Δ " + calcHexDifference().toString().replace(".",Qt.locale().decimalPoint)
            anchors.centerIn: parent
            width: parent.width - units.gu(4)
            wrapMode: Text.WordWrap
            color: newInkColour
            font.pixelSize: appSettings.standardFontSize
            horizontalAlignment: Text.AlignHLeft
        }  // hexDiffLabel

        Label {
            id: changeLabel
            text: hexRectangle.height > changeLabel.height ? i18n.tr("Set background to %1").arg(newBackgroundColour) +"\n" + i18n.tr("Set text to %1").arg(newInkColour) : i18n.tr("Set background to %1").arg(newBackgroundColour) +" - " + i18n.tr("Set text to %1").arg(newInkColour)
            anchors.centerIn: parent
            width: parent.width - units.gu(4)
            wrapMode: Text.WordWrap
            color: newInkColour
            font.pixelSize: appSettings.standardFontSize
            horizontalAlignment: Text.AlignHCenter
        }  // changeLabel

        Rectangle {
            id: swapColorsIcon
            anchors {
                right: parent.right
                rightMargin: units.gu(1)
                verticalCenter: parent.verticalCenter
            }
            height: hexRectangle.height
            width: units.gu(4)
            color: swapClick.pressed ? newInkColour : "transparent"
            MouseArea {
                id: swapClick
                anchors.fill: parent
                onClicked: swapColors()
            }
            Icon {
                height: units.gu(3)
                anchors.centerIn: parent
                color: newInkColour
                width: height
                name: "user-switch"
            }
        }
    } //quickChangeSection

    Rectangle {
        id: topSection
        width: parent.width
        height: (parent.height - (header.height + quickChangeSection.height)) /2
        anchors.top: quickChangeSection.bottom
        color: appSettings.persistentBackgroundColor

        Rectangle {
            id: page1TopHeading
            width: parent.width
            height: parent.height/7 > units.gu(3.5) ? parent.height/7 : units.gu(3.5)
            anchors.top: parent.top
            color: appSettings.persistentBackgroundColor // theme.palette.normal.background

            Rectangle {
                id: topHeadingSpacerTop
                width: parent.width
                height: 1
                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                color: "transparent"
                border.color: Qt.darker(backgroundColour)
            } //topHeadingSpacerTop

            Label {
                text: i18n.tr("Background")
                color: appSettings.persistentInkColor // theme.palette.normal.baseText
                font.pixelSize: appSettings.standardFontSize
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.leftMargin: units.gu(2)
            } //Label

            Label {
                text: newBackgroundColour
                color: appSettings.persistentInkColor  // theme.palette.normal.baseText
                font.pixelSize: appSettings.standardFontSize
                anchors.centerIn: parent
            } //Label

            Rectangle {
                id: topHeadingSpacerBottom
                width: parent.width
                height: 1
                anchors.bottom: parent.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                color: "transparent"
                border.color: Qt.darker(backgroundColour)
            } //topHeadingSpacerBottom

        } //page1TopHeading

        Rectangle {
            id: backgroundRedSliderSection
            width: parent.width
            height: (parent.height - page1TopHeading.height - topSectionSpacer.height) / 3
            anchors.top: page1TopHeading.bottom
            anchors.topMargin: page1TopHeading.height == units.gu(3.5) ? 0 : units.gu(2)
            anchors.bottomMargin: units.gu(1)
            color: parent.color

            Row {
                id: bgSliderRedRow
                width: parent.width - units.gu(4)
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.leftMargin: units.gu(2)
                anchors.rightMargin: units.gu(2)
                spacing: units.gu(2)
                Label {
                    text: i18n.ctr("first letter of color red as capital letter","R")
                    anchors.verticalCenter: parent.verticalCenter
                    color: appSettings.persistentInkColor
                }

                QC.Slider {
                    id: backgroundRedSlider
                    width: parent.width * 0.9
                    handle.height: appSettings.standardFontSize
                    handle.width: handle.height
                    anchors.verticalCenter: parent.verticalCenter
                    from: 0x00
                    value: defaultBackgroundRed
                    to: 0xff
                    stepSize: 0x01
                    onMoved: appSettings.persistentBackgroundRedValue = value
                } //backgroundRedSlider
            }  //bgSliderRedRow

        } //backgroundRedSliderSection

        Rectangle {
            id: backgroundGreenSliderSection
            width: parent.width
            height: (parent.height - page1TopHeading.height - topSectionSpacer.height) / 3
            anchors.top: backgroundRedSliderSection.bottom
            color: parent.color

            Row {
                id: bgSliderGreenRow
                width: parent.width - units.gu(4)
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.leftMargin: units.gu(2)
                anchors.rightMargin: units.gu(2)
                spacing: units.gu(2)
                Label {
                    text: i18n.ctr("first letter of color green as capital letter","G")
                    anchors.verticalCenter: parent.verticalCenter
                    color: appSettings.persistentInkColor
                }

                QC.Slider {
                    id: backgroundGreenSlider
                    width: parent.width * 0.9
                    handle.height: appSettings.standardFontSize
                    handle.width: handle.height
                    anchors.verticalCenter: parent.verticalCenter
                    from: 0x00
                    value: defaultBackgroundGreen
                    to: 0xff
                    stepSize: 0x01
                    onMoved: appSettings.persistentBackgroundGreenValue = value
                } //backgroundGreenSlider
            }  //bgSliderGreenRow
        } //backgroundGreenSliderSection

        Rectangle {
            id: backgroundBlueSliderSection
            width: parent.width
            height: (parent.height - page1TopHeading.height - topSectionSpacer.height) / 3
            anchors.top: backgroundGreenSliderSection.bottom
            color: parent.color

            Row {
                id: bgSliderBlueRow
                width: parent.width - units.gu(4)
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.leftMargin: units.gu(2)
                anchors.rightMargin: units.gu(2)
                spacing: units.gu(2)
                Label {
                    text: i18n.ctr("first letter of color blue as capital letter","B")
                    anchors.verticalCenter: parent.verticalCenter
                    color: appSettings.persistentInkColor
                }

                QC.Slider {
                    id: backgroundBlueSlider
                    width: parent.width * 0.9
                    handle.height: appSettings.standardFontSize
                    handle.width: handle.height
                    anchors.verticalCenter: parent.verticalCenter
                    from: 0x00
                    value: defaultBackgroundBlue
                    to: 0xff
                    stepSize: 0x01
                    onMoved: appSettings.persistentBackgroundBlueValue = value
                } //backgroundBlueSlider
            }  //bgSliderBlueRow
        } //backgroundBlueSliderSection

        Rectangle {
            id: topSectionSpacer
            width: parent.width
            height: 1
            anchors.bottom: parent.bottom
            color: "transparent"
            border.color: Qt.darker(backgroundColour)
        } //topSectionSpacer

    } //topSection

    Rectangle {
        id: bottomSection
        width: parent.width
        height: (parent.height - (header.height + quickChangeSection.height)) /2
        anchors.bottom: parent.bottom
        color: appSettings.persistentBackgroundColor

        Rectangle {
            id: page1BottomHeading
            width: parent.width
            height: parent.height/7 > units.gu(3.5) ? parent.height/7 : units.gu(3.5)
            anchors.top: parent.top
            color: appSettings.persistentBackgroundColor  // theme.palette.normal.background

            Label {
                id: bottomHeadingLabel
                text: i18n.tr("Text")
                color: appSettings.persistentInkColor  // theme.palette.normal.baseText
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.leftMargin: units.gu(2)
                font.pixelSize: appSettings.standardFontSize
            } //bottomHeadingLabel

            Label {
                id: bottomHeadingHex
                text: newInkColour
                color: appSettings.persistentInkColor // theme.palette.normal.baseText
                font.pixelSize: appSettings.standardFontSize
                anchors.centerIn: parent
            } //bottomHeadingHex

            Rectangle {
                id: page1BottomHeadingSpacer
                width: parent.width
                height: 1
                anchors.bottom: parent.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                color: "transparent"
                border.color: Qt.darker(backgroundColour)
            } //page1BottomHeadingSpacer

        } //page1BottomHeading

        Rectangle {
            id: inkRedSliderSection
            width: parent.width
            height: (parent.height - page1BottomHeading.height) / 3
            anchors.top: page1BottomHeading.bottom
            anchors.topMargin: page1BottomHeading.height == units.gu(3.5) ? 0 : units.gu(2)
            color: parent.color

            Row {
                id: inkSliderRedRow
                width: parent.width - units.gu(4)
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.leftMargin: units.gu(2)
                anchors.rightMargin: units.gu(2)
                spacing: units.gu(2)
                Label {
                    text: i18n.ctr("first letter of color red as capital letter","R")
                    anchors.verticalCenter: parent.verticalCenter
                    color: appSettings.persistentInkColor
                }

                QC.Slider {
                    id: inkRedSlider
                    width: parent.width * 0.9
                    handle.height: appSettings.standardFontSize
                    handle.width: handle.height
                    anchors.verticalCenter: parent.verticalCenter
                    from: 0x00
                    value: defaultInkRed
                    to: 0xff
                    stepSize: 0x01
                    onMoved: appSettings.persistentInkRedValue = value
                } //inkRedSlider
            }  //inkSliderRedRow

        } //inkRedSliderSection

        Rectangle {
            id: inkGreenSliderSection
            width: parent.width
            height: (parent.height - page1BottomHeading.height) / 3
            anchors.top: inkRedSliderSection.bottom
            color: parent.color

            Row {
                id: inkSliderGreenRow
                width: parent.width - units.gu(4)
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.leftMargin: units.gu(2)
                anchors.rightMargin: units.gu(2)
                spacing: units.gu(2)
                Label {
                    text: i18n.ctr("first letter of color green as capital letter","G")
                    anchors.verticalCenter: parent.verticalCenter
                    color: appSettings.persistentInkColor
                }

                QC.Slider {
                    id: inkGreenSlider
                    width: parent.width * 0.9
                    handle.height: appSettings.standardFontSize
                    handle.width: handle.height
                    anchors.verticalCenter: parent.verticalCenter
                    from: 0x00
                    value: defaultInkGreen
                    to: 0xff
                    stepSize: 0x01
                    onMoved: appSettings.persistentInkGreenValue = value
                } //inkGreenSlider
            }  //inkSliderGreenRow

        } //inkGreenSliderSection

        Rectangle {
            id: inkBlueSliderSection
            width: parent.width
            height: (parent.height - page1BottomHeading.height) / 3
            anchors.top: inkGreenSliderSection.bottom
            color: parent.color

            Row {
                id: inkSliderBlueRow
                width: parent.width - units.gu(4)
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.leftMargin: units.gu(2)
                anchors.rightMargin: units.gu(2)
                spacing: units.gu(2)
                Label {
                    text: i18n.ctr("first letter of color blue as capital letter","B")
                    anchors.verticalCenter: parent.verticalCenter
                    color: appSettings.persistentInkColor
                }

                QC.Slider {
                    id: inkBlueSlider
                    width: parent.width * 0.9
                    handle.height: appSettings.standardFontSize
                    handle.width: handle.height
                    anchors.verticalCenter: parent.verticalCenter
                    from: 0x00
                    value: defaultInkBlue
                    to: 0xff
                    stepSize: 0x01
                    onMoved: appSettings.persistentInkBlueValue = value
                } //inkBlueSlider
            }  //inkSliderBlueRow

        } //inkBlueSliderSection

    } //bottomSection

    function calcHexDifference() {
        // calculating the contrast difference based on the answer here: https://stackoverflow.com/a/9733420
        // with the details coming from this source: https://www.w3.org/TR/2008/REC-WCAG20-20081211/#contrast-ratiodef
        var ratio = contrast([backgroundRedSlider.value, backgroundGreenSlider.value, backgroundBlueSlider.value], [inkRedSlider.value, inkGreenSlider.value, inkBlueSlider.value])
        return Math.round(ratio*10)/10 // keep one decimal
        // minimal recommended contrast ratio is 4.5, or 3 for larger font-sizes
    }

    function luminance(r, g, b) {
        var a = [r, g, b].map(function (v) {
            v /= 255;
            return v <= 0.03928
                ? v / 12.92
                : Math.pow( (v + 0.055) / 1.055, 2.4 );
        });
        return a[0] * 0.2126 + a[1] * 0.7152 + a[2] * 0.0722;
    }
    function contrast(rgb1, rgb2) {
        var lum1 = luminance(rgb1[0], rgb1[1], rgb1[2]);
        var lum2 = luminance(rgb2[0], rgb2[1], rgb2[2]);
        var brightest = Math.max(lum1, lum2);
        var darkest = Math.min(lum1, lum2);
        return (brightest + 0.05)
             / (darkest + 0.05);
    }

    function setNewColors() {
        appSettings.persistentInkColor = newInkColour
        root.backgroundColor = newBackgroundColour
        appSettings.persistentBackgroundColor = newBackgroundColour
        inkColour = newInkColour
        appSettings.persistentBackgroundRedValue = backgroundRedSlider.value
        appSettings.persistentBackgroundGreenValue = backgroundGreenSlider.value
        appSettings.persistentBackgroundBlueValue = backgroundBlueSlider.value
        appSettings.persistentInkRedValue = inkRedSlider.value
        appSettings.persistentInkGreenValue = inkGreenSlider.value
        appSettings.persistentInkBlueValue = inkBlueSlider.value
    }

    function swapColors() {
        var currentBgRed = backgroundRedSlider.value
        var currentBgGreen = backgroundGreenSlider.value
        var currentBgBlue = backgroundBlueSlider.value
        var currentInkRed = inkRedSlider.value
        var currentInkGreen = inkGreenSlider.value
        var currentInkBlue = inkBlueSlider.value

        backgroundRedSlider.value = currentInkRed
        backgroundGreenSlider.value = currentInkGreen
        backgroundBlueSlider.value = currentInkBlue
        inkRedSlider.value = currentBgRed
        inkGreenSlider.value = currentBgGreen
        inkBlueSlider.value = currentBgBlue
    }

    Component {
        id: contrastErrorDialog

        LTP.Dialog {
            id: contrastDialog
            title: i18n.tr("Possible low contrast")
            contentHeight: units.gu(44)
            contentWidth: units.gu(35)

            Column {
                spacing: units.gu(2)
                Label {
                    id: messageLabel
                    text: i18n.tr("The contrast for text and background colors seems to be low. This might result in a bad visual appearance. Do you really want to set those colors?")
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: parent.width - units.gu(2)
                    wrapMode: Text.WordWrap
                    font.pixelSize: appSettings.standardFontSize
                } //messageLabel

                Button {
                    id: setColorsButton
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: i18n.tr("Set colors")
                    color: theme.palette.normal.negative
                    font.pixelSize: appSettings.standardFontSize
                    visible: calcHexDifference() >= 1.2 ? true : false

                    onClicked: {
                        setNewColors()
                        PopupUtils.close(contrastDialog)
                    } //onClicked
                } //setColorsButton

                Button {
                    id: cancelButton
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: i18n.tr("Cancel")
                    font.pixelSize: appSettings.standardFontSize

                    onClicked: PopupUtils.close(contrastDialog)
                } //cancelButton
            }
        }
    }
}
