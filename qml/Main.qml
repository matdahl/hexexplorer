/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * HexExplorer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import QtQuick.Controls 2.2 as QC
import Lomiri.Components 1.3


MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'hexexplorer.danfro'
    automaticOrientation: true
    backgroundColor: appSettings.persistentBackgroundColor

    width: units.gu(45)
    height: units.gu(75)

    PageStack {
        id: mainPageStack
    }

    // persistent app settings and initial values
    Settings {
        id: appSettings

        property int persistentRedHexValue: 100
        property int persistentGreenHexValue: 64
        property int persistentBlueHexValue: 200

        property int persistentBackgroundRedValue: 0
        property int persistentBackgroundGreenValue: 0
        property int persistentBackgroundBlueValue: 0

        property int persistentInkRedValue: 255
        property int persistentInkGreenValue: 255
        property int persistentInkBlueValue: 255

        property string persistentInkColor: theme.palette.normal.baseText
        property string persistentBackgroundColor: "green"

        // Font Sizes
        property int bigFontSize: standardFontSize * 1.5
        property int standardFontSize: units.gu(2)

    } //persistent app settings

    /* Defining main hex values as properties of root element */

    property string backgroundColour: appSettings.persistentBackgroundColor
    property string inkColour: appSettings.persistentInkColor

    // Popup Colours
    property string popupBackgroundColour: "#CDCDCD" //Silk
    property string popupInkColour: "#666666" //Graphite

    /* end of defining main hex values as properties */

    Component.onCompleted: mainPageStack.push(Qt.resolvedUrl("HexView.qml"))
}
