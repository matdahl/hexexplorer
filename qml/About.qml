/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * HexExplorer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3

Page {
    id: aboutPage
    anchors.fill: parent

    header: PageHeader {
        id: header
        title: i18n.tr('About')

        StyleHints {
            foregroundColor: appSettings.persistentInkColor
            backgroundColor: appSettings.persistentBackgroundColor
            dividerColor: Qt.darker(backgroundColour)
        }
    }

    Sections {
        id: header_sections
        width: parent.width  // needed, otherwise the sections are not horizontally swipeable
        StyleHints {
            selectedSectionColor: appSettings.persistentInkColor
            sectionColor: Qt.darker(backgroundColour)
            underlineColor: Qt.darker(backgroundColour)
        }
        anchors {
            top: header.bottom
            topMargin: units.gu(1)
            horizontalCenter: parent.horizontalCenter
        }
        model: [i18n.tr("General"), i18n.tr("Credits"), i18n.tr("Description")]
    }

    Flickable {
        id: page_flickable

        flickableDirection: Flickable.AutoFlickIfNeeded

        anchors {
            top: header_sections.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        contentHeight:  main_column.height + units.gu(2)
        clip: true

        Column {
            id: main_column

            anchors {
                left: parent.left
                right: parent.right
                top: parent.top
                leftMargin: units.gu(1)
                rightMargin: units.gu(1)
            }

            Item {
                id: icon

                visible: header_sections.selectedIndex === 0
                width: parent.width
                height: app_icon.height + units.gu(4)

                LomiriShape {
                    id: app_icon

                    width: Math.min(aboutPage.width/3, 256)
                    height: width
                    anchors.centerIn: parent

                    source: Image {
                        id: icon_image
                        source: Qt.resolvedUrl("../assets/HexExplorer.png")
                    }
                    radius: "small"
                    aspect: LomiriShape.DropShadow
                }
            }

            Label {
                id: name

                visible: header_sections.selectedIndex === 0
                color: appSettings.persistentInkColor
                text: i18n.tr("HexExplorer") + " v%1".arg(Qt.application.version)
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottomMargin: units.gu(4)
                textSize: Label.Large
                horizontalAlignment:  Text.AlignHCenter
            }

            Repeater {
                id: aboutLinks

                model: [
                { name: i18n.tr("Get the sourcecode"), url: "https://gitlab.com/Danfro/hexexplorer" },
                { name: i18n.tr("Licence") + ": GNU GPL v3", url: "http://www.gnu.org/licenses/" },
                { name: i18n.tr("HexExplorer app in OpenStore"), url: "https://open-store.io/app/danfro.hexexporer" },
                { name: i18n.tr("Report bugs on GitLab"),  url: "https://gitlab.com/Danfro/hexexplorer/issues" },
                { name: i18n.tr("Help with translation via GitLab"), url: "https://gitlab.com/Danfro/hexexplorer/tree/main/po" },
                { name: i18n.tr("View full changelog"), url: "https://gitlab.com/Danfro/hexexplorer/-/blob/main/CHANGELOG.md" }
                ]

                delegate: ListItem {
                    divider { visible: false; }
                    visible: header_sections.selectedIndex === 0
                    height: layoutAbout.height
                    ListItemLayout {
                        id: layoutAbout
                        title.text : modelData.name
                        title.color: appSettings.persistentInkColor
                        ProgressionSlot {name: "external-link"; color: appSettings.persistentInkColor; }
                    }
                    onClicked: Qt.openUrlExternally(modelData.url)
                }
            }

            Rectangle {
                // spacer
                width: parent.width
                height: units.gu(2)
                color: "transparent"
            }

            Repeater {
                id: donateLinks

                model: [
                    { name: i18n.tr("Donate to UBports"), subtitle: i18n.tr("UBports supports the development of Ubuntu Touch."), url: "https://ubports.com/en_EN/donate" },
                    { name: i18n.tr("Donate to me"), subtitle: i18n.tr("I am maintaining and improving this app."),url: "https://paypal.me/payDanfro" }
                ]

                delegate: ListItem {
                    divider { visible: false; }
                    visible: header_sections.selectedIndex === 0
                    height: layoutAbout.height
                    ListItemLayout {
                        id: layoutAbout
                        title.text : modelData.name
                        title.color: appSettings.persistentInkColor
                        subtitle.text: modelData.subtitle
                        subtitle.color: appSettings.persistentInkColor
                        ProgressionSlot {name: "external-link"; color: appSettings.persistentInkColor; }
                    }
                    onClicked: Qt.openUrlExternally(modelData.url)
                }
            }

            Label {
                id: actualdev

                visible: header_sections.selectedIndex === 1
                color: appSettings.persistentInkColor
                text: "\n" + i18n.tr("App development since version v2.0.0")
                anchors.horizontalCenter: parent.horizontalCenter
                font.bold : true
                horizontalAlignment:  Text.AlignHCenter
            }

            ListItem {
                visible: header_sections.selectedIndex === 1
                divider { visible: false; }
                height: l_maintainer.height + divider.height
                ListItemLayout {
                    id: l_maintainer
                    title.text: i18n.tr("Maintainer") + ": Daniel Frost"
                    title.color: appSettings.persistentInkColor
                    ProgressionSlot {name: "external-link"; color: appSettings.persistentInkColor; }
                }
                onClicked: {Qt.openUrlExternally('https://gitlab.com/Danfro')}
            }

            Repeater {
                id: translationsCredits

                model: [
                    { name: i18n.tr("French") + ": Anne Onyme 017"},
                    { name: i18n.tr("Dutch") + ": Heimen Stoffels"},
                    { name: i18n.tr("German") + ": Daniel Frost"}
                ]

                delegate: ListItem {
                    divider { visible: false; }
                    height: layoutAbout.height
                    visible: header_sections.selectedIndex === 1
                    ListItemLayout {
                        id: layoutAbout
                        title.text : modelData.name
                        title.color: appSettings.persistentInkColor
                        ProgressionSlot {name: "external-link"; color: appSettings.persistentInkColor; }
                    }
                    onClicked: Qt.openUrlExternally('https://gitlab.com/Danfro/hexexplorer/tree/main/po')
                }
            }

            Rectangle {
                // spacer
                width: parent.width
                height: units.gu(2)
                color: "transparent"
            }

            Label {
                id: historicaldev

                visible: header_sections.selectedIndex === 1
                color: appSettings.persistentInkColor
                text: "\n" + i18n.tr("App development up to version v1.2.0")
                anchors.horizontalCenter: parent.horizontalCenter
                font.bold : true
                horizontalAlignment:  Text.AlignHCenter
            }

            Repeater {
                id: historicalDevCredits

                model: [
                    { name: i18n.tr("Author") + ": perryhelion", url: "https://github.com/perryhelion" }
                ]

                delegate: ListItem {
                    divider { visible: false; }
                    visible: header_sections.selectedIndex === 1
                    height: layoutAbout.height
                    ListItemLayout {
                        id: layoutAbout
                        title.text : modelData.name
                        title.color: appSettings.persistentInkColor
                        ProgressionSlot {name: "external-link"; color: appSettings.persistentInkColor; }
                    }
                    onClicked: Qt.openUrlExternally(modelData.url)
                }
            }

            // App description as in OpenStore page and README
            Label {
                id: descriptionHeader

                visible: header_sections.selectedIndex === 2
                color: appSettings.persistentInkColor
                text: i18n.tr("App description")
                anchors.horizontalCenter: parent.horizontalCenter
                font.bold : true
                horizontalAlignment:  Text.AlignHCenter
            }
            Label {
                id: description

                visible: header_sections.selectedIndex === 2
                color: appSettings.persistentInkColor

                horizontalAlignment: Text.AlignJustify //Text.AlignHCenter
                width: parent.width //- units.gu(2)
                text: "\n"
                    + i18n.tr("Move the sliders, see the colours, read the hexadecimal representation. Try the colors with the apps background and text color settings.")
                    + "\n\n"
                    + i18n.tr("This is a fairly simple program that might prove useful to anyone unfamiliar with the 16 million or so colours available to them when programming in any computer language that uses the hexadecimal numbers to represent the three colour components of the RGB system.")
                    + "\n\n"
                    + i18n.tr("Each colour component has 256 different values (from 0 to 255) and can be 'mixed' together to make pretty much any colour in any shade or hue that you could want.")
                    + "\n\n"
                    + i18n.tr("The original developer made the app as a learning exercise for him to see how straightforward or not it was to program in QML. He wanted to use the Ubuntu SDK mainly using mainly Qt Quick Controls instead of 'Ubuntu Components 1.3' (although still present to make use of 'i18n.tr()' functions). This should theoretically make the app more easily portable to other platforms.")
                    + "\n\n"
                wrapMode: Text.WordWrap
            }
            Label {
                id: purposeHeader

                visible: header_sections.selectedIndex === 2
                color: appSettings.persistentInkColor
                text: i18n.tr("Re-release purposes")
                anchors.horizontalCenter: parent.horizontalCenter
                font.bold : true
                horizontalAlignment:  Text.AlignHCenter
            }
            Label {
                id: purpose

                visible: header_sections.selectedIndex === 2
                color: appSettings.persistentInkColor

                horizontalAlignment: Text.AlignJustify //Text.AlignHCenter
                width: parent.width //- units.gu(2)
                text: "\n"
                    + i18n.tr("The original creator does not maintain the app anymore. The main purpose of this re-release is to keep the app maintained. One fundamental change is the switch to a pure qml type app dropping the unneeded c++ parts. This does make the app architecture independed. It is also implementing back the Ubuntu Touch look and feel with header, as well as adding some features and fixing some bugs. The code is migrated from github to gitlab.")
                wrapMode: Text.WordWrap
            }
        }
    }
}
