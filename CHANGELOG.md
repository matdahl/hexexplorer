v2.1.3
- add: focal support

v2.1.2
- update: Dutch and French translations, thanks @Vistaus and @Anne17

v2.1.1
- fix: visibility issues in landscape

v2.1.0
- add: button to quickly swap text and background color
- add: check to avoid possible low contrast between text and background
- add: color initials to sliders
- fix: set initial slider colors

v2.0.0.
- add: migrate the app to Gitlab
- add: new maintainer Daniel Danfro Frost
- add: allow copy of hex code
- add: about page
- add: German translation
- fix: made the app arch all instead of being architecture dependend by removing c++ code
- fix: settings behavior
- fix: use UT style header and icons
